﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BlackFade : MonoBehaviour
{
    public SpriteRenderer SpriteRenderer;
    public float FadeToValue = 0;
    public float FadeSpeed = 1;

    public void StartFade()
    {
        StartCoroutine(FadeTo(FadeToValue, FadeSpeed));
    }

    IEnumerator FadeTo(float aValue, float aTime)
    {
        float alpha = SpriteRenderer.color.a;
        for (float t = 0.0f; t < 1.0f; t += Time.deltaTime / aTime)
        {
            Color newColor = new Color(1, 1, 1, Mathf.Lerp(alpha, aValue, t));
            SpriteRenderer.color = newColor;
            yield return null;
        }
    }
}
