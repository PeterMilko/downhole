﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.UI;
using DG.Tweening;

public class ScoreUI : MonoBehaviour
{
    public TextMeshProUGUI BestScore;
    public TextMeshProUGUI CurrentScore;
    public Image ScoreImage;
    public DOTweenAnimation Tween;
    public AudioSource IncramentSound;
    public GameObject HighScoreParticles;
    private bool ShowHighScoreParticles = true; // dont do fire works if 1st run 
    
    private float incramentSoundStartVolume;



    void Start()
    {
        BestScore.text = "Best: " + GameManager.instance.BestScore.ToString();
        if (GameManager.instance.BestScore < 10)
        {
            ShowHighScoreParticles = false;
            Debug.Log(ShowHighScoreParticles);
        }
        incramentSoundStartVolume = IncramentSound.volume;
    }

    public void HighScore()
    {
        if (ShowHighScoreParticles == false)
        {
            return;
        }
        CancelInvoke("ResetIncramentPitch");
        CurrentScore.color = Color.yellow;
        //ScoreImage.color = Color.yellow;
        IncramentSound.Play();
        if (IncramentSound.pitch < 2.5f)
        {
            IncramentSound.pitch += 0.1f;
        }
        Invoke("ResetIncramentPitch", 1.5f);

        if (!IsInvoking("ResetScoreImage"))
        {
            Invoke("ResetScoreImage", 0.5f);

            Tween.DORestart(); // restarting to shake
            if (HighScoreParticles != null && ShowHighScoreParticles)
            {
                HighScoreParticles.SetActive(true);
            }
        }
    }

    void ResetIncramentPitch()
    {
        IncramentSound.pitch = 1;
        IncramentSound.volume = incramentSoundStartVolume;
    } 

    void ResetScoreImage()
    {
        CurrentScore.color = Color.white;
        //ScoreImage.color = Color.black;
    }

    void Update()
    {
        CurrentScore.text = GameManager.instance.CurrentBestDepth.ToString();
        BestScore.text = "Best: " + GameManager.instance.BestScore.ToString();
    }
}
