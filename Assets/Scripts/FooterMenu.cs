﻿using DG.Tweening;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class FooterMenu : MonoBehaviour
{
    public GameObject GameModeButton;
    public GameObject RestartButton;
    public GameObject CharactersButton;
    public GameObject FreeReviveButton;
    public GameObject ReviveButton;
    public TextMeshProUGUI ReviveText;
    public GameObject ChestButton;
    public DOTweenAnimation ShowTween;
    [Header("Character Window")]
    public GameObject CharacterWindow;
    public DOTweenAnimation CharacterWindowTween;

    [HideInInspector]
    public Windows CurrentWindow = Windows.None;
    public enum Windows
    { None, Character };

    void Start()
    {
        ShowFooterMenu(false);
    }

    public void ChestPressed()
    {

    }

    public void GameModePressed()
    {

    }

    public void RestartPressed()
    {
        if (GameManager.instance.Dead)
        {
            GameManager.instance.Restart();
        }
    }

    public void ToggleWindow(Windows currentWindow)
    {

    }

    public void ToggleCharactersWindow()
    {
        if (CurrentWindow != Windows.Character)
        {
            CurrentWindow = Windows.Character;
            OpenWindow();
        }
        else
        {
            CloseWindow();
            CurrentWindow = Windows.None;
        }      
    }



    public void OpenWindow()
    {
        switch (CurrentWindow)
        {
            case Windows.None:
                //Do nothing
                break;
            case Windows.Character:
                CharacterWindow.SetActive(true);
                CharacterWindowTween.DORestart();
                break;
            default:
                break;
        }
    }

    public void CloseWindow()
    {
        switch (CurrentWindow)
        {
            case Windows.None:
                //Do nothing
                break;
            case Windows.Character:
                CharacterWindowTween.DOPlayBackwards();
                break;
            default:
                break;
        }
    }

    public void ShowFooterMenu(bool ShowRevive = false)
    {
        if (GameManager.instance.Revives > 0)
        {
            ReviveText.text = "Use 1 of " + GameManager.instance.Revives + " Revives";
        }
        else
        {
            ReviveText.text = "Buy 5 Revives!";
        }
        gameObject.SetActive(true);
        RestartButton.SetActive(GameManager.instance.Dead);
        ShowTween.DORestart();
        FreeReviveButton.SetActive(ShowRevive);
    }

    public void HideFooterMenu()
    {
        ShowTween.DOPlayBackwards();
    }

    void Update()
    {
        
    }
}
