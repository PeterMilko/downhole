﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MoveRigidBody : MonoBehaviour
{
    public float Speed = 0;
    public Rigidbody2D Rigidbody;
    public Collider2D Trigger;

    private bool holdStill = false;

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.tag == "Player")
        {
            // hurt player
        }
        if (collision.transform.tag == "Platform" || collision.transform.tag == "Enemy" || collision.transform.tag == "Wall")
        {
            if (!IsInvoking("Turning"))
            {
                Vector3 newScale = transform.parent.localScale;
                newScale.x *= -1;
                transform.parent.localScale = newScale;
                Speed *= -1;
                Trigger.enabled = false;
                Invoke("Turning",0.5f);
            }
            else
            {
                holdStill = true;
            }   
        }
    }

    private void Turning()
    {
        Trigger.enabled = true;
        holdStill = false;
    }

    void Update()
    {
        if (holdStill == false)
        {
            transform.parent.Translate(transform.right * Time.deltaTime * Speed);
        }    
    }
}
