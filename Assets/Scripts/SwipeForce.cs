﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SwipeForce : MonoBehaviour
{

    public Rigidbody2D RigidBody;
    public SwipeCoolDownUI SwipeCoolDownUI;
    public LayerMask PlatformLayerMask;
    public AudioEvent audioEventJump;

    private Vector3 startpos;
    private Vector3 endpos;
    private Vector3 forceVector;
    private bool canJump = false;

    private void Start()
    {
        //AudioEventJump = AudioContainer.GetEvent("Jump");
    }

    private void OnTriggerStay2D(Collider2D collision)
    {
        if (collision.tag == "Platform" || collision.tag == "Enemy" || collision.tag == "Wall")
        {
            canJump = true;
            //SwipeCoolDownUI.Slider.value = 100;
        }
    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        if (collision.tag == "Platform" || collision.tag == "Enemy" || collision.tag == "Wall")
        {
            canJump = false;
         
        }
    }

    private void FixedUpdate()
    {
        //Debug.Log(RigidBody.velocity.magnitude);
        if (RigidBody.velocity.magnitude > GameManager.instance.MaxVelocity)
        {
            
            RigidBody.velocity += (-RigidBody.velocity) * 0.1f;
        }
    }

    void Update()
    {
        if(canJump && !GameManager.instance.Dead)
        {
            if (Input.GetMouseButtonDown(0))
            {
                startpos = Input.mousePosition;
            }

            if (Input.GetMouseButtonUp(0))
            {
                
                endpos = Input.mousePosition;
                
                forceVector = endpos - startpos;
                //&& SwipeCoolDownUI.CanSwipe
                float distance = Vector3.Distance(endpos, startpos);
                if (distance > 50 )
                {
                    audioEventJump.Play(transform.position);
                    //SwipeCoolDownUI.StartCoolDown();
                    RigidBody.AddForce((new Vector3(forceVector.x/1.5f, forceVector.y,forceVector.z) * GameManager.instance.ForcePower));
                }
            }
        }       
    }
}
