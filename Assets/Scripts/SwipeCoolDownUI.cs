﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SwipeCoolDownUI : MonoBehaviour
{
    public Transform PlayerTransform;
    public Slider Slider;
    public bool CanSwipe = true;

    void Start()
    {
        Slider.gameObject.SetActive(false);
    }

    public void StartCoolDown()
    {
        Slider.gameObject.SetActive(true);
        Slider.value = 0;
        CanSwipe = false;
    }
    void Update()
    {
        if (Slider.value < 100)
        {
            Slider.value += Time.deltaTime * GameManager.instance.SwipeCoolDownSpeed;
        }
        else
        {
            CanSwipe = true;
            Slider.gameObject.SetActive(false);
        }

        this.transform.position = PlayerTransform.position + new Vector3(0f,1f,0f);
    }
}
