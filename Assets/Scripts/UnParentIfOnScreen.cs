﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UnParentIfOnScreen : MonoBehaviour
{
    private float SpawnY;

    void Start()
    {
        SpawnY = transform.position.y;
    }

    void Update()
    {
        if (transform.position.y < SpawnY - 20)
        {
            transform.parent = null;
        }
    }
}
