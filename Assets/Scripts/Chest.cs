﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Chest : MonoBehaviour
{
    public Animator Animator;
    public GameObject Particles;
    public GameObject[] Rewards;
    public Collider2D Trigger;

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.tag == "Player")
        {
            Invoke("GiveReward",0.4f);
            Trigger.enabled = false;
            Animator.SetBool("OpenChest",true);
            Particles.SetActive(true);
            this.enabled = false;
        }
    }

    private void GiveReward()
    {
        GameManager.instance.ScreenShake(0.2f,4);
        GameObject prefab = Rewards[Random.Range(0, Rewards.Length)];
        Instantiate(prefab, transform.position, Quaternion.identity);
    }
}
