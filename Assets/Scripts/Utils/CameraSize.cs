﻿using Cinemachine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraSize : MonoBehaviour
{
    public CinemachineVirtualCamera CinemachineVirtualCamera;
    public SpriteRenderer ScreenSizeSprite;
    void Start()
    {
        float orthagraphicSize;
        if (Screen.height > Screen.width)
        {
             orthagraphicSize = ScreenSizeSprite.bounds.size.x * Screen.height / Screen.width * 0.5f;
        }
        else
        {
             orthagraphicSize = ScreenSizeSprite.bounds.size.y / 1.5f;
        }

        CinemachineVirtualCamera.m_Lens.OrthographicSize = orthagraphicSize;
    }
}
