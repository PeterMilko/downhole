﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using DG.Tweening;

public class TextBubble : MonoBehaviour
{
    public TextMeshProUGUI Text;
    public DOTweenAnimation Tween;

    public void Show(string dialogue)
    {
        this.gameObject.SetActive(true);
        Tween.DORestart();
        Text.text = dialogue;
    }

    public void Hide()
    {
        Tween.DOPlayBackwards();
    }
}
