﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SoundControls : MonoBehaviour
{
    public AudioSource AudioSource;
    public AudioClip[] AudioClips;
    public float pitchVariation;
    public bool PlayOnAwake = false;

    void Start()
    {
        if (PlayOnAwake)
        {
            PlaySound();
        }
    }

    public void PlaySound()
    {
        AudioSource.clip = AudioClips[Random.Range(0, AudioClips.Length)];
        AudioSource.pitch = 1 + (Random.Range(-pitchVariation, pitchVariation));
        AudioSource.Play();
    }
}
