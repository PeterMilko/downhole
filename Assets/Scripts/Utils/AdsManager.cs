﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Advertisements;

public class AdsManager : MonoBehaviour, IUnityAdsListener
{
    public static AdsManager instance;

    void Awake()
    {
        if (instance == null)
            instance = this;
        else if (instance != this)
            Destroy(this);    
    }

    void Start()
    {
        Advertisement.AddListener(this);
#if UNITY_IOS
    Advertisement.Initialize("3509663");    
#elif UNITY_ANDROID
        Advertisement.Initialize("3509662");
#endif
        DontDestroyOnLoad(this.gameObject);
    }

    public void ShowSkipableVideoAd()
    {
            Advertisement.Show();
    }

    public void ShowRewardedVideoAd(string videoString)
    {
        Debug.Log("Clicked");
        if (Advertisement.IsReady(videoString))
        {
            Advertisement.Show(videoString);
        }
        else
        {
            Debug.Log("Not Ready");
            GameManager.instance.VideoAdFailed();
        }   
    }

    //Ads listener

    public void OnUnityAdsReady(string placementId)
    {
        
    }

    public void OnUnityAdsDidError(string message)
    {
      
    }

    public void OnUnityAdsDidStart(string placementId)
    {
        
    }

    public void OnUnityAdsDidFinish(string placementId, ShowResult showResult)
    {
        if (showResult == ShowResult.Finished)
        {
            Debug.Log("Finished");

            switch (placementId)
            {
                case "rewardedReviveVideo":
                    GameManager.instance.StartRevivePlayer();
                    break;
                default:
                    break;
            }
            // Reward the user for watching the ad to completion.
        }
        else if (showResult == ShowResult.Skipped)
        {
            GameManager.instance.VideoAdFailed();
        }
        else if (showResult == ShowResult.Failed)
        {
            GameManager.instance.VideoAdFailed();
        }
    }
}
