﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SoundControl : MonoBehaviour
{
    public AudioSource[] Sounds;
    public float pitchVariation;
    public bool PlayOnAwake = false;

    void Start()
    {
        if (PlayOnAwake)
        {
            PlaySound();
        }
    }

    public void PlaySound()
    {
        AudioSource sound = Sounds[Random.Range(0, Sounds.Length)];
        sound.pitch = 1 + (Random.Range(-pitchVariation, pitchVariation));
        sound.Play();
    }
}
