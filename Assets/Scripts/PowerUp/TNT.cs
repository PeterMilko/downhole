﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TNT : MonoBehaviour
{
    public static List<TNT> TNTArray = new List<TNT>();

    public float Durration = 10;
    public float CurrentDuration;
    public ParticleSystem SmokeParticleSystem;
    public GameObject Explosion;

    private ParticleSystem.MainModule psMain;

    void Start()
    {
        if (TNTArray.Count > 0)
        {
            foreach (TNT TNTScript in TNTArray)
            {
                Durration += TNTScript.CurrentDuration;
            }
        }
        GameManager.instance.Player.Smashed += Explode;
        Invoke("EndPower", Durration);
        InvokeRepeating("CountDown",0,1f);
        psMain = SmokeParticleSystem.main;
        psMain.duration = Durration;
        CurrentDuration = Durration;
        SmokeParticleSystem.Play();
        TNTArray.Add(this);
    }

    void CountDown()
    {
        CurrentDuration--;
    }

    void EndPower()
    {
        TNTArray.Remove(this);
        Destroy(gameObject,2);
    }

    void Explode()
    {
        Instantiate(Explosion,this.transform.position, Quaternion.identity);
    }

    private void OnDestroy()
    {
        GameManager.instance.Player.Smashed -= Explode;
    }
}
