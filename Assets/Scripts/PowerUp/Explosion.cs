﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Explosion : MonoBehaviour
{
    public string[] Tags;

    private void OnTriggerEnter2D(Collider2D collision)
    {
        foreach (string tag in Tags)
        {
            if (collision.tag == tag)
            {        
               collision.GetComponent<Health>().TakeDamage();
            }
        }
    }
}
