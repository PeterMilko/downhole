﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Heart : MonoBehaviour
{
    private float height;
    private bool triggered = false;
    public Collider2D Trigger;

    private void Start()
    {
        
       
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.tag == "Player")
        {
            collision.GetComponentInParent<Player>().Hearts.Add(this.gameObject);
            GameManager.instance.Player.Health++;
            height = GameManager.instance.Player.Health-1f;
            triggered = true;
            Trigger.enabled = false;
        }
    }

    private void Update()
    {
        if (triggered)
        {
            this.transform.position = GameManager.instance.Player.transform.position + new Vector3(0, height, 0);
        }
    }
}
