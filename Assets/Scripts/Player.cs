﻿
using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class Player : MonoBehaviour
{
    public int Health = 1;
    public List<GameObject> Hearts;
    public bool Invincible = false;
    public GameObject HitParticle;
    public GameObject DieParticleSystem;
    public Sprite DieSprite; 
    public SpriteRenderer SpriteRenderer;
    public Shader NormalShader;
    public Shader WhiteShader;
    public Transform CollisionParticleTransform;
    public ParticleSystem CollisionParticleSystem;
    public ScoreUI ScoreUI;

    public delegate void MyDelegate();
    public MyDelegate TookDamage;

    public delegate void SmashDelegate();
    public SmashDelegate Smashed;

    public AudioEvent audioEventHitGround;

    private Rigidbody2D Rigidbody;
    private bool flashing = false;

    void Start()
    {
        SpriteRenderer.material.shader = NormalShader;
        Rigidbody = this.GetComponent<Rigidbody2D>();
    }

    public void MakeInvincible(int duration)
    {
        Invincible = true;
        InvokeRepeating("Flash", 0, 0.2f);
        Invoke("EndInvincible", duration);
    }

    private void Flash()
    {
        if (flashing)
        {
            flashing = false;
            SpriteRenderer.material.shader = NormalShader;
        }
        else
        {
            flashing = true;
            SpriteRenderer.material.shader = WhiteShader;
        }
    }

    public void TakeDamage()
    {
        if (Invincible == false)
        {
            Invincible = true;
            Invoke("EndFlash", 0.2f);
            SpriteRenderer.material.shader = WhiteShader; 
            if (TookDamage != null)
            {
                TookDamage.Invoke();
           
            }
            if (Health > 1)
            {
                GameManager.instance.ScreenShake(0.2f,4);
                Invoke("EndInvincible", 1f);
                Health--;
                GameObject removeHeart = Hearts[Hearts.Count-1];
                Hearts.Remove(removeHeart);
                Destroy(removeHeart);
                GameObject ParticleObject = Instantiate(HitParticle);
                ParticleObject.transform.position = this.transform.position;
            }
            else
            {
                GameObject ParticleObject = Instantiate(DieParticleSystem);
                ParticleObject.transform.position = this.transform.position;
                SpriteRenderer.sprite = DieSprite;
                GameManager.instance.GameOver();   
            }
           
        }   
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        CollisionParticleTransform.transform.position = collision.GetContact(0).point;
        CollisionParticleSystem.Play();


        if (Vector3.Dot(Rigidbody.velocity, collision.relativeVelocity) > 1)
        {
            audioEventHitGround.Play(transform.position);
        }

        if (Vector3.Dot(Rigidbody.velocity, collision.relativeVelocity) > 15)
        {
            if (Smashed != null) Smashed.Invoke();
            GameManager.instance.ScreenShake(0.2f, 1);
        }
    }

    void EndFlash()
    {
        SpriteRenderer.material.shader = NormalShader;
    }

    void EndInvincible()
    {
        CancelInvoke("Flash");
        SpriteRenderer.material.shader = NormalShader;
        Invincible = false;
    }


    void Update()
    {
        if (this.transform.position.y*-1 > GameManager.instance.CurrentBestDepth)
        {
            GameManager.instance.CurrentBestDepth = (int)this.transform.position.y * -1;
        }
        if (GameManager.instance.CurrentBestDepth > GameManager.instance.BestScore)
        {
            GameManager.instance.BestScore = GameManager.instance.CurrentBestDepth;
            ScoreUI.HighScore();
        }
    }   
}
