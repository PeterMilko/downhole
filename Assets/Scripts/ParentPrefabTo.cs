﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ParentPrefabTo : MonoBehaviour
{
    public GameObject Prefab;
    public float TriggerDelay = 0;
    public Collider2D Trigger;
    public bool RandomRotation = false;

    private void Start()
    {
        if (TriggerDelay > 0)
        {
            Trigger.enabled = false;
            Invoke("EnableTrigger", TriggerDelay);
        }
    }

    private void EnableTrigger()
    {
        Trigger.enabled = true;
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.tag == "Player")
        {
            GameObject prefabObject =  Instantiate(Prefab, collision.transform);
            if (RandomRotation)
            {
                var euler = transform.eulerAngles;
                euler.z = Random.Range(0.0f, 180f);
                prefabObject.transform.eulerAngles = euler;
            }
            Destroy(this.gameObject);
        }
    }
}
