﻿using UnityEngine;

public class AutoSetOrthographicSize : MonoBehaviour
{
	public new Camera camera;

	void Awake()
	{
		// 540 is the magic number
		// 1080 x 1920			1.77 x 540 = 960			
		// 1080 x 2160			2	x 540 = 1080			
		// 1440 x 2960			2.055  x 540 = 1111                   
		float h = Screen.height;
		float w = Screen.width;
		float ratio = h / w;
		camera.orthographicSize = (ratio * 540);
	}
}
