﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class OnTriggerEnterDo : MonoBehaviour
{
    public Collider2D Trigger;
    public UnityEvent Event;
    public bool Once = false;
    public void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.tag == "Player")
        {
            Event.Invoke();
            if (Once)
            {
                this.enabled = false;
            }
        }   
    }
}
