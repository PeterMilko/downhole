﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DestroyPos : MonoBehaviour {

	private void OnTriggerEnter2D(Collider2D collision)
	{
		if (collision.tag == "Enemy")
		{
			Destroy(gameObject,0.1f);
		}
	}
}
