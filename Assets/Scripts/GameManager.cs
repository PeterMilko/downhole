﻿using UnityEngine;
//using UnityEngine.Rendering.PostProcessing;
using UnityEngine.SceneManagement;
using TMPro;
using UnityEngine.UI;
using Cinemachine;
using UnityEngine.Audio;
using System.Collections;
using UnityEngine.Rendering;
//using UnityEngine.Rendering.Universal;

public class GameManager : MonoBehaviour
{
    public int CurrentLevel = 1;
    public float SwipeCoolDownSpeed = 2;
    public float ForcePower = 0.7f;
    public float MaxVelocity = 10;
    public int CurrentBestDepth = 0;
    public int BestScore = 0;
    public int Coins = 0;
    public int Revives = 0;
    public bool EndlessMode = false;
    public Player Player;
    public bool Dead = false;
    public SpriteRenderer PlayerSprite;
    public bool Revived = false;
    public Sprite StartSprite;
    public Sprite FrogSprite;
    public Sprite Enchae;
    public Sprite Narwhal;
    public CinemachineVirtualCamera VirtualCamera;
    private CinemachineBasicMultiChannelPerlin virtualCameraNoise;

    [Header("UI STUFF")]
    public GameObject TitleCanvas;
    public Menus Menus;
    public FooterMenu FooterMenu;
    public TextMeshProUGUI MenuTitle;
    public Button ExitPausebutton;
    
    //Events
    public delegate void UpdatedCoinsDelegate(int num);
    public UpdatedCoinsDelegate UpdatedCoins;
    public bool Showtitle = true;

    [Header("OTHER CLASSES")]
    // PostProcess
    //public PostProcessVolume PostProcessVolume;
    public Volume Volume;
    //private ChromaticAberration chromaticAberration;
    //private DepthOfField depthOfField;
    //Sound
    public AudioMixer Mixer;
    public float SFXVolume;
    //Ads
    public AdsManager AdsManager;

    private bool paused = false;
    private int CharactersUnlocked = 0;
 

    public static GameManager instance;

    [RuntimeInitializeOnLoadMethod]
    static void OnRuntimeMethodLoad()
    {
        //Show title then have a quest menu 
        if (instance.Showtitle)
        {
            instance.TitleCanvas.SetActive(true);
        }     
    }
    private void Awake()
    {
        if (instance == null)
            instance = this;
        else if (instance != this)
            Destroy(this);
    }

    public void CloseGame()
    {
        Application.Quit();
    }

    private void Start()
    {
        //PostProcessVolume.profile.TryGetSettings(out chromaticAberration);
        //Volume.profile.TryGet<ChromaticAberration>(out chromaticAberration);
        //Volume.profile.TryGet<DepthOfField>(out depthOfField);

        //PostProcessVolume.profile.TryGetSettings(out depthOfField);
        //chromaticAberration.active = false;
        //depthOfField.active = false;
        LoadSave();
        Player.enabled = true;
        UpdatePlayerSprite();

        virtualCameraNoise = VirtualCamera.GetCinemachineComponent<Cinemachine.CinemachineBasicMultiChannelPerlin>();
        virtualCameraNoise.m_AmplitudeGain = 0;
        virtualCameraNoise.m_FrequencyGain = 0;

        AdsManager = AdsManager.instance;
        Mixer.GetFloat("SFXVolume", out SFXVolume);
        //Delete SaveFile
        //PlayerPrefs.DeleteAll();
    }

    private void UpdatePlayerSprite()
    {
        switch (CharactersUnlocked)
        {
            case 0:
                PlayerSprite.sprite = StartSprite;
                break;
            case 1:
                PlayerSprite.sprite = FrogSprite;
                break;
            case 2:
                PlayerSprite.sprite = Enchae;
                break;
            case 3:
                PlayerSprite.sprite = Narwhal;
                break;
            default:
                PlayerSprite.sprite = StartSprite;
                break;
        }
    }

    public void ScreenShake(float duration, int extra)
    {
        virtualCameraNoise.m_AmplitudeGain = 1+ extra;
        virtualCameraNoise.m_FrequencyGain = 2+ extra;
        Invoke("EndScreenShake", duration);
    }

    public void VideoAdFailed()
    {
        //show some ui window "video failed to load, maybe your wifi is off"
        Menus.ShowPopUpMessage("Video failed", 2, Color.red);
    }

    public void ShowRewardedVideoAd(string videoString)
    {
        AdsManager.ShowRewardedVideoAd(videoString);
    }

    public void RewardedVideoWasSkipped()
    {
        // Say sorry wont give you reward because you skipped
        Menus.ShowPopUpMessage("You skipped video", 2, Color.red);
    }

    private void EndScreenShake()
    {
        virtualCameraNoise.m_AmplitudeGain = 0;
        virtualCameraNoise.m_FrequencyGain = 0;
    }

    private void SaveGame()
    {
        if (EndlessMode == true)
        {
            PlayerPrefs.SetInt("BestEndlessScore", BestScore);
        }
        else
        {
            PlayerPrefs.SetInt("BestScore", BestScore);
        }
        PlayerPrefs.SetInt("Coins", Coins);
        PlayerPrefs.SetInt("Revives", Revives);
        PlayerPrefs.SetInt("CurrentLevel", CurrentLevel);
        PlayerPrefs.Save();
    }

    private void LoadSave()
    {
        if (EndlessMode == true)
        {
            if (PlayerPrefs.HasKey("BestEndlessScore")) BestScore = PlayerPrefs.GetInt("BestEndlessScore");
        }
        else
        {
            if (PlayerPrefs.HasKey("BestScore")) BestScore = PlayerPrefs.GetInt("BestScore");
        }
        if (PlayerPrefs.HasKey("CurrentLevel")) CurrentLevel = PlayerPrefs.GetInt("CurrentLevel");
        if (PlayerPrefs.HasKey("Coins")) Coins = PlayerPrefs.GetInt("Coins");
        if (PlayerPrefs.HasKey("Revives")) Revives = PlayerPrefs.GetInt("Revives");
        if (PlayerPrefs.HasKey("CharactersUnlocked")) CharactersUnlocked = PlayerPrefs.GetInt("CharactersUnlocked");
    }

    public void UpdateCoins(int amount)
    {
        Coins += amount;
        if (UpdatedCoins != null)
        {
            //Debug.Log("UpdatedCoins event");
            UpdatedCoins(amount);
        }
       
    }

    public void GameOver()
    {
        Menus.PauseButton.SetActive(false);
        Dead = true;
        Time.timeScale = 0.3f;
        //chromaticAberration.active = true;
        MenuTitle.text = "Game Over";  
        ExitPausebutton.enabled = false;
        Invoke("ShowMenu", 0.5f);
        StartCoroutine("FadeSFXVolume");
    }
    
    private void ShowMenu()
    {
        FooterMenu.ShowFooterMenu(!Revived);
        Player.enabled = false;
    }

    IEnumerator FadeSFXVolume()
    {
        float volume;
        Mixer.GetFloat("SFXVolume", out volume);
        while (volume > -50)
        {
            SetSFXVolume(volume);
            volume--;
            yield return new WaitForSecondsRealtime(.1f);
        }
        Time.timeScale = 0f;

        //if (volume > -50)
        //{
        //    SetSFXVolume(volume-1);
        //}
        //else
        //{
        //    Time.timeScale = 0.0f;
        //    CancelInvoke("FadeSFXVolume");
        //}
        //yield return null;
    }

    public void SetSFXVolume(float volume)
    {
        Mixer.SetFloat("SFXVolume", volume);
    }

    public void TogglePause()
    {
        if (paused)
        {
            paused = false;
            Time.timeScale = 1;
            //depthOfField.active = false;
        }
        else
        {
            MenuTitle.text = "Paused";
            ExitPausebutton.enabled = true;
            paused = true;
            Time.timeScale = 0;
            //depthOfField.active = true;
        }       
    }

    public void UseRevive()
    {
        if (Revives > 0)
        {
            Revives--;
            PlayerPrefs.SetInt("Revives", Revives);
            PlayerPrefs.Save();
        }
        else
        {
            //IAPManager.instance.Buy5Revives();
        }
        StartRevivePlayer();
    }

    public void StartRevivePlayer()
    {
        FooterMenu.HideFooterMenu();
        FooterMenu.FreeReviveButton.SetActive(false);
        Menus.ShowPopUpMessage("GET READY!",2, Color.yellow);
        StopCoroutine("FadeSFXVolume");
        StartCoroutine("RevivePlayer");
        SetSFXVolume(SFXVolume);
    }

    IEnumerator RevivePlayer()
    {
        yield return new WaitForSecondsRealtime(2f);
        Player.enabled = true;
        Player.Health = 1;
        Player.MakeInvincible(3);
        SetSFXVolume(SFXVolume);
         //PostProcessVolume.profile.TryGetSettings(out chromaticAberration);
         //PostProcessVolume.profile.TryGetSettings(out depthOfField);
        //chromaticAberration.active = false;
        //depthOfField.active = false;
        paused = false;
        Time.timeScale = 1;
         //chromaticAberration.active = false;
         //depthOfField.active = false;
        Dead = false;
        Revived = true;
        UpdatePlayerSprite();
        Menus.PauseButton.SetActive(true);
    }

    public void Restart()
    {
        SaveGame();
        SetSFXVolume(SFXVolume);
         //PostProcessVolume.profile.TryGetSettings(out chromaticAberration);
         //PostProcessVolume.profile.TryGetSettings(out depthOfField);
        
        //chromaticAberration.active = false;
        //depthOfField.active = false;
        paused = false;
        Time.timeScale = 1;
         //chromaticAberration.active = false;
         //depthOfField.active = false;
        Dead = false;
        SceneManager.LoadScene(0);
        SetSFXVolume(SFXVolume);
    } 
    
    public void BuyFrog()
    {
        if (Coins >=50)
        {
            Coins -= 50;
            PlayerPrefs.SetInt("CharactersUnlocked", 1);
            PlayerSprite.sprite = FrogSprite;
            SaveGame();
            Restart();
        }
       
    }
    
    public void BuyEnchae()
    {
        if (Coins >= 100)
        {
            Coins -= 100;
            PlayerPrefs.SetInt("CharactersUnlocked", 2);
            PlayerSprite.sprite = Enchae;
            SaveGame();
            Restart();
        }
        
    }

    public void BuyNarwhal()
    {
        if (Coins >= 200)
        {
            Coins -= 200;
            PlayerPrefs.SetInt("CharactersUnlocked", 3);
            PlayerSprite.sprite = Narwhal;
            SaveGame();
            Restart();
        }    
    }

}
