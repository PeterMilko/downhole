﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using DG.Tweening;

public class Menus : MonoBehaviour
{
    public GameObject PopUpMessage;
    public GameObject PauseButton;
    public DOTweenAnimation Tween;
    private int _duration;

    private IEnumerator coroutine;

    public void ShowPopUpMessage(string message, int duration, Color TextColor)
    {
        PopUpMessage.SetActive(true);
        TextMeshProUGUI Text = PopUpMessage.GetComponent<TextMeshProUGUI>();
        Text.text = message;
        Text.color = TextColor;
        Tween.DORestart();
        _duration = duration;
        coroutine = HidePopUpMessage();
        StartCoroutine(coroutine);
        //HidePopUpMessage();
    }

    IEnumerator HidePopUpMessage()
    {
        Debug.Log("Hide");
        yield return new WaitForSeconds(_duration);
        PopUpMessage.SetActive(false);
        Debug.Log("Hid");
    }
}
