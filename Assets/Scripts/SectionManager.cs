﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class SectionManager : MonoBehaviour
{
    public Transform PlayerTransform;
    //private int Section = 1;
    public Transform LowestSectionTransform;
    public Menus PopUpMessage;
    public GameObject[] SectionPrefabs;
    private List<GameObject> ExistingSections = new List<GameObject>();
    private System.Random Ran;

    void Start()
    {
      
        if (GameManager.instance.EndlessMode)
        {
            Ran = new System.Random();
        }
        else
        {
            Ran = new System.Random(GameManager.instance.CurrentLevel); //Random.InitState(#) PlayerPrefs.GetInt(Level);   
            PopUpMessage.ShowPopUpMessage("Level " + GameManager.instance.CurrentLevel.ToString(), 5, Color.white);
        }
        MakeNewSection();
    }

    private void MakeNewSection()
    {
        int randomNumber = Ran.Next(0, SectionPrefabs.Length);
        GameObject prefab = SectionPrefabs[randomNumber];
        GameObject newSection = Instantiate(prefab, LowestSectionTransform.position + new Vector3(0f,-10f,0f), Quaternion.identity,this.transform);
        LowestSectionTransform = newSection.transform;
        ExistingSections.Add(newSection);
        if (ExistingSections.Count > 5)
        {
            Destroy(ExistingSections[0].transform.gameObject);
            ExistingSections.RemoveAt(0);
        }
    }

    private void FinishLevel()
    {
        GameManager.instance.CurrentLevel++;
        GameManager.instance.Restart();
    }

    void Update()
    {
        if (PlayerTransform.position.y <= LowestSectionTransform.position.y+20)
        {
            MakeNewSection();
        }
        if (GameManager.instance.EndlessMode == false)
        {
            if (GameManager.instance.CurrentBestDepth >= 100)
            {
                FinishLevel();             
            }
           
        }
    }
}
