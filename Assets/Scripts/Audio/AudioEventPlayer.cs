using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AudioEventPlayer : MonoBehaviour
{
    [SerializeField]
    AudioEvent audioEventOnAwake, audioEventOnDestroy;

    void Awake()
    {
        if (audioEventOnAwake != null)
            audioEventOnAwake.Play(transform.position);
    }

    private void OnDestroy() {
        if (audioEventOnDestroy != null)
            audioEventOnDestroy.Play(transform.position);

        // TODO: Add functionality for stopping looped audio event (the one that started on awake)
    }
}
