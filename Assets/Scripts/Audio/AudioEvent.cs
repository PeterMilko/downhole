using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Audio;

[System.Serializable, CreateAssetMenu(fileName = "AudioEvent", menuName = "AudioEvents/AudioEvent")]
public class AudioEvent : ScriptableObject
{
    static GameObject pool;

    public bool positional = true;
    public bool loop = false;
    [Range(0f, 1f)]
    public float volume = 1.0f;
    public float pitch = 1f;
    public float randomPitch = 0f;
    public float cooldownSeconds = 0.0f;

    public List<AudioClip> clips;
    public AudioMixerGroup outputGroup;

    private int clipIndex, lastPlayedIndex;
    private float lastPlayedTime, nextAvailablePlayTime; // Used for cooldown

    [HideInInspector] public event System.Action OnPlay;

    private void UpdateAudioSourceProperties(AudioSource src) {
        src.volume = volume;
        src.loop = loop;
        src.outputAudioMixerGroup = outputGroup;
        src.pitch = pitch;
        src.playOnAwake = false;
        src.spatialBlend = positional ? 0.75f : 0f;
    }

    public void RandomizeIndex() { RandomizeIndex(0, clips.Count); }

    // Randomize the index from (inclusive) to y (exclusive)
    public void RandomizeIndex(int from, int to)
    {
        int newIndex;
        do
        {
            newIndex = Random.Range(from, to);
        } while (clips.Count > 1 && newIndex == lastPlayedIndex);

        clipIndex = newIndex;
    }

    private AudioSource GetAvailableSourceFromPool() {
        if (pool == null) {
            pool = GameObject.Find("AudioSourcePool");
            if (pool == null) {
                Debug.Log("AudioEvent: Couldn't find Audio Source Pool object.");
                return null;
            } 
        }

        for (int i = 0; i < pool.transform.childCount; i++) {
            AudioSource src = pool.transform.GetChild(i).GetComponent<AudioSource>();
            if (!src.isPlaying)
                return src;
        }

        // If none are available, pick random one
        return pool.transform.GetChild(Random.Range(0, pool.transform.childCount)).GetComponent<AudioSource>();
    }

    public virtual void Play(Vector3 position, bool oneShot = false)
    {
        AudioSource source = GetAvailableSourceFromPool();

        if (source == null)
        {
            Debug.Log($"AudioEvent: Tried to play {name} with source equal to null.");
            return;
        }

        UpdateAudioSourceProperties(source);

        // Check if AudioSource is active and check cooldown time
        if (!source.gameObject.activeSelf)
        {
            Debug.Log("Sound did not play because the source gameobject was not active.");
            return;
        }

        source.transform.position = position;
        source.clip = clips[clipIndex];
        source.pitch = pitch + Random.Range(0f, randomPitch) - (randomPitch * 0.5f);

        // Play
        source.Play();

        lastPlayedIndex = clipIndex;
        RandomizeIndex();

        lastPlayedTime = Time.time;
        nextAvailablePlayTime = lastPlayedTime + cooldownSeconds;

        OnPlay?.Invoke();
    }

    public void SetIndex(int index) { clipIndex = index; }

    public void SetPitch(float pitch) { this.pitch = pitch; }
}