﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FirstTime : MonoBehaviour
{
    public bool Show;
    public bool AfterFirstTimeShow;
    void Start()
    {
        //CheckScore();
        Invoke("CheckScore", 0.2f);     
    }

    void CheckScore()
    {
        if (GameManager.instance.BestScore < 11)
        {
           gameObject.SetActive(Show);
        }
        else
        {
            gameObject.SetActive(AfterFirstTimeShow);
        }
    }

}
