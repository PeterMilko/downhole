﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RandomSpawn : MonoBehaviour
{
    public GameObject[] PrefabArray;
    [Range(0, 100)]
    public int SpawnChance = 100; //percent
    void Start()
    {
        if (Random.Range(1,101) < SpawnChance)
        {
            GameObject prefab = PrefabArray[Random.Range(0, PrefabArray.Length)];
            GameObject newSection = Instantiate(prefab, this.transform);
        }
       
    }
}
