﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class DoEvent : MonoBehaviour
{
    public float DelayEvent = 0;
    public UnityEvent Event;
    
    void Start()
    {
            Invoke("InvokeEvent", DelayEvent);    
    }

    void InvokeEvent()
    {
        Event.Invoke();
    }

}
