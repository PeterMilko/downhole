﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class Health : MonoBehaviour
{
    public int HP = 1;
    public GameObject DestroyParticlePrefab;
    public UnityEvent DamagedEvent;
    public UnityEvent KilledEvent;

    public void TakeDamage()
    {
        HP--;
        if (HP <= 0)
        {
            GameObject ParticleObject = Instantiate(DestroyParticlePrefab, transform.position, Quaternion.identity);
            if (KilledEvent != null)
            {
                KilledEvent.Invoke();
            }
            Destroy(this.gameObject);
        }
        else
        {
            if (DamagedEvent != null)
            {
                DamagedEvent.Invoke();
            }
        }   
    }
}
