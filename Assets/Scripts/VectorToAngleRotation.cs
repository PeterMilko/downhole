﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine; 

public class VectorToAngleRotation : MonoBehaviour
{
    public void Rotate(Vector3 direction)
    {
        transform.eulerAngles = new Vector3(0,0, GetAngleFromVector(direction));
    }

    //maybe make static
    private float GetAngleFromVector(Vector3 direction)
    {
        Vector3 _direction = direction.normalized;
        float n = Mathf.Atan2(_direction.y, direction.x) * Mathf.Rad2Deg;
        if (n < 0) n += 360;
        return n;
    }
}
