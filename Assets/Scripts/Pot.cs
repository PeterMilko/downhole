﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Pot : MonoBehaviour
{ 
    [Header("Have them match up")]
    public Sprite[] Sprites;
    public Color[] PotColors;
    public GameObject ShatterParticle;
    public GameObject CoinPrefab;

    private int potNumber;
    private int potParticleColorNumber;
    private SpriteRenderer spriteRenderer;

    void Start()
    {
        potNumber = Random.Range(0, Sprites.Length);
        spriteRenderer = transform.GetComponent<SpriteRenderer>();
        spriteRenderer.sprite = Sprites[potNumber];
        potParticleColorNumber = potNumber;
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.tag == "Player")
        {
            GameObject shatterParticle = Instantiate(ShatterParticle, this.transform.position, Quaternion.identity);
            ParticleSystem.MainModule psMain;
            ParticleSystem ps = shatterParticle.GetComponent<ParticleSystem>();
            psMain = ps.main;
            psMain.startColor = PotColors[potNumber];
            ps.Play();
            if (Random.Range(0, 100) > 60)
            {
                Instantiate(CoinPrefab, this.transform.position, Quaternion.identity);
            }       
            Destroy(gameObject);
        }
    }
}
