﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CrumbleBlock : MonoBehaviour
{
    public Animator Animator;
    public SpriteRenderer SpriteRenderer;
    public GameObject DestroyParticlePrefab;
    public Health Health;
    public SoundControl SoundControl;

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.tag == "Player")
        {
            Animator.SetBool("Crumble", true);
            Invoke("Crumble",1f);
            SoundControl.PlaySound();
        }
    }

    public void Crumble()
    {
        Health.TakeDamage();
    }
}
