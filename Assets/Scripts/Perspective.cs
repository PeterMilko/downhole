﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Perspective : MonoBehaviour
{

    private float scale;

    void Update()
    {   
        if (GameManager.instance.Player.transform.position.y < this.transform.position.y)
        {
            scale = Mathf.Min( (this.transform.position.y - GameManager.instance.Player.transform.position.y)*0.5f, 1);
            transform.localScale = new Vector2(1, scale);
        }
       
    }
}
