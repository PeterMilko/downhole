﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TopBlocker : MonoBehaviour
{
    void Update()
    {
        if (GameManager.instance.Player.transform.position.y*-1 > GameManager.instance.CurrentBestDepth)
        {
            this.transform.position = new Vector2(0, GameManager.instance.Player.transform.position.y+18);
        }       
    }
}
