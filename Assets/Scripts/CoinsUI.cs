﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using DG.Tweening;

public class CoinsUI : MonoBehaviour
{
    public TextMeshProUGUI CoinsText;
    public DOTweenAnimation Tween;

    void Start()
    {
        GameManager.instance.UpdatedCoins += UpdateCoinUI;
        CoinsText.text = GameManager.instance.Coins.ToString();
    }

    void UpdateCoinUI(int amount)
    {
        Tween.DORestart();
        CoinsText.text = GameManager.instance.Coins.ToString();
    }
   
    void Update()
    {
        
    }

    private void OnDestroy()
    {
        GameManager.instance.UpdatedCoins -= UpdateCoinUI;
    }
}
