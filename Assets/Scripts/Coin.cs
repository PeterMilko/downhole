﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class Coin : MonoBehaviour
{
    public DOTweenAnimation Tween;
    public Collider2D Collider;
    public GameObject DestroyParticlePrefab;
    public float TriggerDelay = 0;

    private void Start()
    {
        if (TriggerDelay > 0)
        {
            Collider.enabled = false;
            Invoke("EnableTrigger", TriggerDelay);
        }
    }

    private void EnableTrigger()
    {
        Collider.enabled = true;
    }

    public void Destroy()
    {
        if (DestroyParticlePrefab != null)
        {
            GameObject DestroyParticleObject = Instantiate(DestroyParticlePrefab);
            DestroyParticleObject.transform.position = this.transform.position;
        }
        Destroy(this.gameObject);
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.tag == "Player")
        {
            Collider.enabled = false;
            GameManager.instance.UpdateCoins(1);
            Tween.DORestart();
        }     
    }
}
