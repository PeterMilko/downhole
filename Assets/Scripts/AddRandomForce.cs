﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AddRandomForce : MonoBehaviour
{
    public Rigidbody2D Rigidbody2D;
    public float Power = 1;

    void Start()
    {
        float x = Random.Range(-3, 3);
        float y = Random.Range(1, 5);
        Vector2 forceDirection = new Vector2(x,y);
        Rigidbody2D.AddForce(forceDirection* Power);
    }

    void Update()
    {
        
    }
}
