﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class StalactiteTrigger : MonoBehaviour
{
    public Collider2D Trigger;
    public GameObject DamageTrigger;
    public Rigidbody2D RigidBody;
    public float FallDelay = 1;
    public UnityEvent TriggeredEvent;
    public UnityEvent FallEvent;
    public Shader NormalShader;
    public Shader WhiteShader;
    public SpriteRenderer SpriteRenderer;

    private void Start()
    {
        RigidBody.isKinematic = true;
        DamageTrigger.SetActive(false);
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.tag == "Platform")
        {
           
        }
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.tag == "Player")
        {
            TriggeredEvent.Invoke();
            Invoke("Flash", FallDelay*0.5f);
            Invoke("Fall", FallDelay);
            Invoke("ChangeLayer", FallDelay);
        }
    }

    void Flash()
    {
        SpriteRenderer.material.shader = WhiteShader;
    }

    void Fall()
    {
        SpriteRenderer.material.shader = NormalShader;
        FallEvent.Invoke();
        RigidBody.isKinematic = false;
        DamageTrigger.SetActive(true);  
    }

    void ChangeLayer()
    {
        //this.transform.gameObject.layer = LayerMask.NameToLayer("Coin");
        Destroy(this.gameObject);
    }
}
