﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Music : MonoBehaviour
{
    public AudioSource AudioSource;
    public AudioClip[] AudioClip;

    void Start()
    {
        AudioSource.clip = AudioClip[Random.Range(0, AudioClip.Length)];
        AudioSource.Play();
    }
}
