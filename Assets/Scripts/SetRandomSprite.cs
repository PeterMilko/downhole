﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SetRandomSprite : MonoBehaviour
{
    [Range(0, 100)]
    public int SpawnChance = 100;
    public SpriteRenderer SpriteRenderer;
    public Sprite[] Sprites;
    public bool RandomlyFlip = false;

    void Start()
    {
        if (Random.Range(1, 101) < SpawnChance)
        {
            SpriteRenderer.sprite = Sprites[Random.Range(0, Sprites.Length)];
            if (RandomlyFlip)
            {
                //50% chance
                if (Random.value < .5)
                {
                    SpriteRenderer.flipX = true;
                }          
            }
        }
        else
        {
            this.gameObject.SetActive(false);
        }
    }
}
