﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;



public class PlatformBlock : MonoBehaviour
{
    public enum blockType { Dirt, Stone };
    public blockType BlockType = blockType.Dirt;
    public SpriteRenderer SpriteRenderer;
    public Sprite[] Sprites;
    public Health Health;

    private Vector3 startpos;
    private Vector3 endpos;

    //Audio
    public SoundControl SoundControl;

    private void Start()
    {
        SpriteRenderer.sprite = Sprites[Random.Range(0, Sprites.Length)];
        //if (AudioContainer != null)
        //{
        //    TapSound = AudioContainer.GetEvent("TapSound");
        //}    
    }
    private void OnMouseDown()
    {
        startpos = Input.mousePosition;
    }

    private void OnMouseUp()
    {
        endpos = Input.mousePosition;
        float distance = Vector3.Distance(endpos, startpos);
        if (distance < 70 && !GameManager.instance.Dead)
        {
            switch (BlockType)
            {
                case blockType.Dirt:
                    DestroyBlock();
                    break;
                case blockType.Stone:
                    SoundControl.PlaySound();
                    break;
                default:
                    break;
            }
        }   
    }

    private void DestroyBlock()
    {
        Health.TakeDamage();
    }
}
