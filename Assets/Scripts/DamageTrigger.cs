﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DamageTrigger : MonoBehaviour
{
    public bool DamageEnemies = false;

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.tag == "Player")
        {
            collision.transform.parent.GetComponent<Player>().TakeDamage();
        }
        if (DamageEnemies && collision.tag == "Enemy" )
        {
            //Debug.Log(collision.transform.parent);
            collision.GetComponent<Health>().TakeDamage();          
        }
    }
}
