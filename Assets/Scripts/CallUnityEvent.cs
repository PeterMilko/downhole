﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class CallUnityEvent : MonoBehaviour
{
    public bool CallOnStart = false;
    public UnityEvent Event;

    void Start()
    {
        if (CallOnStart)
        {
            CallEvent();
        }
    }

    public void CallEvent()
    {
        Event.Invoke();
    }
}
